import pl.sda.streamy.DataCollections;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {

        System.out.println("=============================NAZWISKA==================================");

//        1) Wszystkie nazwiska o d³ugoci co najwy¿ej 4 znaków, zapisane wielkimi literami
        List<String> krotkieNazwiskaWielkimiLiterami = DataCollections.getSurnames().stream()
                .filter(n -> n.length() <= 4)
                .map(String::toUpperCase)
                .collect(Collectors.toList());
        System.out.println(krotkieNazwiskaWielkimiLiterami);


//        2) Wszystkie nazwiska zaczynaj¹ce siê na literê 'B'
        List<String> nazwiskaNaLitereB = DataCollections.getSurnames().stream()
                .filter(n -> n.startsWith("B"))
                .collect(Collectors.toList());
        System.out.println(nazwiskaNaLitereB);

//        3) Pocz¹tkowe trzy litery wszystkich nazwisk, bez powtórzeñ, z ma³ych liter
        Set<String> trzyPierwszeLiteryNazwisk = DataCollections.getSurnames().stream()
                .map(n -> n.substring(0, 3).toLowerCase())
                .collect(Collectors.toCollection(TreeSet::new));
        System.out.println(trzyPierwszeLiteryNazwisk);

//        4) 10 najd³u¿szych nazwisk, posortowanych malej¹co wed³ug d³ugoci
        List<String> najdluzsze = DataCollections.getSurnames().stream()
                .sorted(Comparator.comparingInt(String::length).reversed())
                .limit(10)
                .collect(Collectors.toList());
        System.out.println(najdluzsze);

//        4*) TODO: Obs³u¿ miejsca "ex aequo"

//        5) 20 najkrótszych nazwisk, posortowanych wed³ug ostatniej litery


        List<String> najkrotsze = DataCollections.getSurnames().stream()
                .sorted(Comparator.comparingInt(String::length))
                .limit(20)
                .sorted(Comparator.comparingInt(s -> Character.toLowerCase(s.charAt(s.length() - 1))))
                .collect(Collectors.toList());
        System.out.println(najkrotsze);


//        6) Odwróæ kolejnoæ liter we wszystkich nazwiskach i pozstaw jedynie te, które maj¹ literê 'A' wsród pierwszych trzech liter (odwróconego nazwiska)
        List<String> szesc = DataCollections.getSurnames().stream()
                .map(s -> new StringBuilder(s).reverse().toString())
                .filter(s -> s.substring(0, 3).toLowerCase().contains("a"))
                .collect(Collectors.toList());
        System.out.println(szesc);


//        7) Policz, ile jest nazwisk zaczynaj¹cych siê na ka¿d¹ z liter alfabetu (rezultat jako Map<Character, Integer>)
        Map<Character, Integer> siedem = DataCollections.getSurnames().stream()
                .map(s -> s.charAt(0))
                .collect(Collectors.toMap(c -> c, c -> 1, (v1, v2) -> v1 + v2));
        System.out.println(siedem);


//        8*) Jaka litera pojawia siê najczeciej we wszystkich nazwiskach?

        Map<Character, Integer> literaWystapienia = DataCollections.getSurnames().stream()
                .flatMap(s -> s.toLowerCase().chars().mapToObj(c -> (char) c))
//                .forEach(System.out::println);
                .collect(Collectors.toMap(c -> c, c -> 1, (c1, c2) -> c1 + c2));

        System.out.println(literaWystapienia);
        System.out.println(literaWystapienia.entrySet().stream()
                .max((e1, e2) -> e1.getValue() > e2.getValue() ? 1 : -1)
                .get()
                .getKey());

        // ==========================================================================================


        System.out.println("=============================LICZBY==================================");

//        1) Ile jest liczb parzystych?
        System.out.println(DataCollections.getNumbers(10_000).stream()
                .filter(n -> n % 2 == 0)
                .count());


//        2) Ile jest liczb piêciocyfrowych?
        System.out.println(DataCollections.getNumbers(10_000).stream()
                .filter(n -> (n > 9_999 && n < 1_000_000))
                .mapToInt(n -> 1)
                .sum());


//        3) Jaka jest najwiêksza i najmniejsza liczba?
        IntSummaryStatistics summaryStatistics = DataCollections.getNumbers(100_000).stream()
                .mapToInt(n -> n)
                .summaryStatistics();

        System.out.println("Max: " + summaryStatistics.getMax());
        System.out.println("Min: " + summaryStatistics.getMin());


//        4) Jaka jest ró¿nica miêdzy najwiêksza a najmniejsz¹ liczb¹?

        System.out.println("Roznica: " + (summaryStatistics.getMax() - summaryStatistics.getMin()));


//        5) Jaka jest rednia wszystkich liczb?
        System.out.println(DataCollections.getNumbers(100_000).stream()
                .mapToInt(n -> n)
                .average().getAsDouble());


//        6*) Jaka jest mediana wszystkich liczb?

        System.out.println(DataCollections.getNumbers(10_000).stream()
                .sorted()
                .skip(5_000)
                .findFirst().get());

//      trudniejsza metoda - ale prawidlowa* mediana
        List<Integer> sortedNumbers = DataCollections.getNumbers(10_000).stream()
                .sorted()
                .collect(Collectors.toList());

        Stream<Integer> stream = sortedNumbers.stream();

        if (sortedNumbers.size() % 2 == 0) {
            OptionalDouble mediana = stream.skip((sortedNumbers.size() / 2) - 1).limit(2).mapToInt(n -> n).average();
            System.out.println(mediana.getAsDouble());
        } else {
            Optional<Integer> medianaProsta = stream.skip(sortedNumbers.size() / 2).findFirst();
            System.out.println(medianaProsta.get());
        }


//        8*) Ile jest wyst¹pieñ ka¿dej cyfry (rezultat jako Map<Integer, Integer> z kluczami od 0 do 9)

        Map<Integer, Integer> cyfraWystapien = DataCollections.getNumbers(10_000).stream()
                .map(String::valueOf)
                .flatMap(s -> s.chars().mapToObj(n -> n - 48))
                .collect(Collectors.toMap(n -> n, n -> 1, (n1, n2) -> n1 + n2));

//        7*) Jaka cyfra pojawia siê najczêciej we wszystkcih liczbach?

        System.out.println("--7--");
        System.out.println(cyfraWystapien.entrySet().stream()
                .max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1)
                .get()
                .getKey());


        //        9) Wypisz wszystkie liczby pierwsze, posortowane rosn¹co

        long start = System.currentTimeMillis();
        System.out.println(DataCollections.getNumbers(600_000).parallelStream()
                .mapToInt(n -> n)
                .filter(Main::isPrime)
                .sorted()
                //                .forEach(System.out::println);
                .count());

        System.out.println((System.currentTimeMillis() - start) + " ms");


        System.out.println("=============================LITERY==================================");


//            1) Ile jest wszystkich s³ów?

        System.out.println("--1--");
        System.out.println(DataCollections.getLoremIpsum().stream()
                .count());

//            2) Ile s³ów zaczyna siê na literê 'D'?

        System.out.println("--2--");
        System.out.println(DataCollections.getLoremIpsum().stream()
                .filter(s -> s.toUpperCase().startsWith("D"))
                .count());
//                .mapToInt(s -> 1)
//                .sum());

//            3) Policz, ile jest s³ów o danej d³ugoci (Map<Integer, Integer>)

        System.out.println("--3--");
        Map<Integer, Integer> ileSlowDanejDlugosci = DataCollections.getLoremIpsum().stream()
                .collect(Collectors.toMap(s -> s.length(), s -> 1, (s1, s2) -> s1 + s2));
        System.out.println(ileSlowDanejDlugosci);


//            4) Jaka litera pojawia siê narzadziej?

        System.out.println("--4--");
        System.out.println(DataCollections.getLoremIpsum().stream()
                .map(s -> s.replaceAll("[.,]", ""))
                .flatMap(s -> s.toLowerCase().chars().mapToObj(c -> (char) c))
                .collect(Collectors.toMap(c -> c, c -> 1, (c1, c2) -> c1 + c2))
                .entrySet().stream()
                .min((e1, e2) -> e1.getValue() > e2.getValue() ? 1 : -1)
                .get().getKey());


//            5*) Ile jest s³ów, które posiadaj¹ dwie identyczne litery obok siebie (np. 'g' w "debugger")?

        System.out.println("--5--");
        System.out.println(DataCollections.getLoremIpsum().stream()
                .map(s -> s.replaceAll("[.,]", ""))
                .filter(s -> {
                    for (int i = 0; i < s.length() - 1; i++) {
                        if (s.charAt(i) == s.charAt(i + 1)) {
                            return true;
                        }
                    }
                    return false;
                }).count());


//            6**) Ile jest s³ów, które s¹ palindromami?

        System.out.println("--6--");
        System.out.println(DataCollections.getLoremIpsum().stream()
                .map(s -> s.replaceAll("[.,]", ""))
                .filter(s -> {
                    for (int i = 0; i < s.length() / 2; i++) {
                        if (s.charAt(i) != s.charAt(s.length() - 1 - i)) {
                            return false;
                        }
                    }
                    return true;
                }).count());

    }

    public static boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }

        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }


}
